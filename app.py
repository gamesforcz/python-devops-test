from flask import Flask
from redis import Redis, RedisError
import psycopg2
from psycopg2.extras import RealDictCursor
from datetime import datetime
import os

postgressDB = 'count'
postgressUser = 'defaultuser'
postgressPassword = 'defaultuser123'
redisPort = '6379'

try:
    postgressDB = os.environ['POSTGRES_DB']
    postgressUser = os.environ['POSTGRES_USER']
    postgressPassword = os.environ['POSTGRES_PASSWORD']
    redisPort = os.environ['REDIS_APP_Port']
except KeyError:
    pass

app = Flask(__name__)
redis = Redis(host='redis', port=redisPort, db=0, decode_responses=True)

def get_db_connection():
    conn = psycopg2.connect(host='postgres',
                            dbname=postgressDB,
                            user=postgressUser,
                            password=postgressPassword)
    return conn

def create_visits_table():
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute('''
        CREATE TABLE IF NOT EXISTS visits (
            id SERIAL PRIMARY KEY,
            timestamp TIMESTAMP NOT NULL
        );
    ''')
    conn.commit()
    cur.close()
    conn.close()

def create_tables():
    with app.app_context():
        create_visits_table()

# Manually push the application context and create tables
create_tables()

@app.route('/')
def index():
    # Check if the 'hits' key exists in Redis, if not, create it with a value of 0
    try:
        count = redis.incr('hits')
    except RedisError:
        count = 'Redis server not available'
    
    # Get current time
    now = datetime.now()
    formatted_date = now.strftime('%Y-%m-%d %H:%M:%S')

    # Insert into PostgreSQL
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute('INSERT INTO visits (timestamp) VALUES (%s)', (formatted_date,))
    conn.commit()
    cur.close()
    conn.close()

    return f'<h1>Visit Count: {count}</h1><h2>Last Visit Time: {formatted_date}</h2>'

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
