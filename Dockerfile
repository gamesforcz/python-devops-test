FROM python:3.8-slim

WORKDIR /app

COPY . /app

RUN pip install Flask psycopg2-binary redis

CMD ["python", "app.py"]